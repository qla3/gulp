// *************************************************************************
// USTAWIENIA MODUŁÓW
// *************************************************************************

var conf_obserwuj_pliki = [
    '*.scss',
    '!node_modules/**'
];

var conf_przetwarzaj_pliki = [
    'style.scss',
];

var conf_autoprefixer = {
    browsers: ['> 0.5%', 'last 2 versions'],
    cascade: true,
    remove: true
};

var conf_comments = {
    preserve: true
};

var conf_combine_mq = {
    beautify: false
};

var conf_css_nano = {
    zindex: false
};

var conf_source_map = '.';

var conf_dest = '.';

// *************************************************************************
// WŁĄCZANIE MODUŁÓW
// *************************************************************************
var turn_sourcemap = false;
var turn_stripCssComments = true;
var turn_cssnano = true;

// *************************************************************************
// KONIEC USTAWIEŃ
// *************************************************************************

var gulp = require('gulp');
var sass = require('gulp-sass');
var gulpif = require('gulp-if');
var rename = require('gulp-rename');
var autoprefixer = require('gulp-autoprefixer');
var combineMq = require('gulp-combine-mq');
var stripCssComments = require('gulp-strip-css-comments');
var cssnano = require('gulp-cssnano');
var sourcemaps = require('gulp-sourcemaps');

gulp.task('test', function() {});

gulp.task('watch', function() {
    gulp
        .watch(conf_obserwuj_pliki, ['sass'])
        .on('change', function(event) {
            console.log('File ' + event.path + ' was ' + event.type + ', running tasks...');
        });
});


gulp.task('sass', function() {
    return gulp
        .src(conf_przetwarzaj_pliki)
        .pipe(gulpif(turn_sourcemap, sourcemaps.init()))
        .pipe(sass({
            includePaths: ['node_modules/susy/sass']
        }).on('error', sass.logError))
        // .resume()
        .pipe(gulpif(turn_stripCssComments, stripCssComments(conf_comments)))
        .pipe(combineMq(conf_combine_mq))
        .pipe(autoprefixer(conf_autoprefixer))
        .pipe(gulpif(turn_cssnano, cssnano(conf_css_nano)))
        .pipe(rename({
            extname: '.css'
        }))
        .pipe(gulpif(turn_sourcemap, sourcemaps.write(conf_source_map)))
        .pipe(gulp.dest(function (file) {
            return file.base;
        }));
});


// Default Task
gulp.task('default', ['sass', 'watch']);
